## qssi-user 13 TKQ1.220807.001 00WW_3_390 release-keys
- Manufacturer: hmd global
- Platform: holi
- Codename: SCW_sprout
- Brand: Nokia
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 5.4.210
- Id: TKQ1.220807.001
- Incremental: 00WW_3_390
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Nokia/ScarletWitch_00WW/SCW_sprout:13/TKQ1.220807.001/00WW_3_390:user/release-keys
- OTA version: 
- Branch: qssi-user-13-TKQ1.220807.001-00WW_3_390-release-keys
- Repo: nokia/SCW_sprout
